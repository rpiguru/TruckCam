# TruckCam

Truck Camera Surveillance System based on Raspberry Pi

## Components

- Raspberry Pi 3B

- ELP 1megapixel Day Night Vision Indoor&Outdoor CCTV USB Dome Housing Camera

    https://www.amazon.com/ELP-1megapixel-Vandal-proof-Industrial-Security-cctv/dp/B00VFLWOC0
