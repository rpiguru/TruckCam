import glob
import ntpath
import os
import signal
import subprocess
import threading
import time

from settings import GPIO_TRUCK, VIDEO_DIR, SAVE_INTERVAL, VIDEO_SAVE_CMD
from utils.aws import upload_to_aws, remove_old_s3_files
from utils.common import logger, is_rpi, get_serial, check_internet_connection, check_running_proc, kill_process_by_name


if is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_TRUCK, GPIO.IN, pull_up_down=GPIO.PUD_UP)


class TruckCam(threading.Thread):

    def __init__(self):
        super().__init__()
        self._last_gpio_active_time = -1
        threading.Thread(target=upload_videos).start()
        threading.Thread(target=remove_old_s3_files).start()

    def run(self) -> None:
        while True:
            if not is_rpi or GPIO.input(GPIO_TRUCK):
                if self._last_gpio_active_time > 0 and time.time() - self._last_gpio_active_time > 1:
                    if not check_running_proc('ffmpeg'):
                        cmd = VIDEO_SAVE_CMD.format(duration=SAVE_INTERVAL * 60, path=VIDEO_DIR)
                        my_env = os.environ.copy()
                        my_env["FFREPORT"] = f"file={os.path.join(VIDEO_DIR, 'ffmpeg.log')}:level=32"
                        logger.info(f"Starting recorder: {cmd}")
                        subprocess.Popen(cmd, shell=True, env=my_env)
                elif self._last_gpio_active_time < 0:
                    self._last_gpio_active_time = time.time()
            else:
                self._last_gpio_active_time = -1
                if check_running_proc('ffmpeg'):
                    kill_process_by_name('ffmpeg', sig=signal.SIGINT)
            time.sleep(.5)


def upload_videos():
    while True:
        if check_internet_connection():
            for video_file in glob.glob(os.path.join(VIDEO_DIR, "*.mp4")):
                if time.time() - os.path.getmtime(video_file) > SAVE_INTERVAL * 60:
                    logger.info(f"Trying to upload - {video_file}")
                    s3_file = f"{get_serial()}/{ntpath.basename(video_file)}"
                    if upload_to_aws(local_file=video_file, s3_file=s3_file):
                        os.remove(video_file)
        time.sleep(1)


if __name__ == '__main__':

    logger.info("========== Starting TruckCam ==========")

    cam = TruckCam()
    cam.start()
