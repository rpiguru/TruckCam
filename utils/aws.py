import time
from datetime import datetime

import boto3
from botocore.exceptions import ClientError

from settings import AWS_SECRET_KEY, AWS_ACCESS_KEY_ID, AWS_BUCKET
from utils.common import logger, check_internet_connection


def upload_to_aws(local_file, s3_file='target_file'):
    s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)
    try:
        result = s3.upload_file(local_file, AWS_BUCKET, s3_file)
        logger.info(f"Upload: {local_file}, result: {result}")
        return True
    except Exception as e:
        logger.error(f"Failed to upload - {e}")


def download_from_aws(s3_file, local_file):

    s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)
    try:
        s3.download_file(AWS_BUCKET, s3_file, local_file)
        print('Downloaded')
    except ClientError:
        return


def remove_old_s3_files():
    s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)
    while True:
        if check_internet_connection():
            try:
                file_list = s3.list_objects(Bucket=AWS_BUCKET)['Contents']
                for f in file_list:
                    elapsed_days = (datetime.now() - f['LastModified'].replace(tzinfo=None)
                                    ).total_seconds() // 3600 // 24
                    if elapsed_days > 30:
                        logger.info(f"{f['Key']} is older than a month, deleting...")
                        s3.delete_object(Bucket=AWS_BUCKET, Key=f['Key'])
            except Exception as e:
                logger.error(f"Failed to remove old files - {e}")
        time.sleep(3600)


if __name__ == '__main__':

    # upload_to_aws(local_file='test.txt', s3_file='test/test.txt')
    # download_from_aws(s3_file='test/test.txt', local_file='downloaded.txt')

    remove_old_s3_files()
