import gc
import os
import signal
import socket
import subprocess
import sys
import logging.config
import netifaces
import platform
import http.client


_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


try:
    is_rpi = 'arm' in os.uname()[4]
except (AttributeError, IndexError):
    is_rpi = False


logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('TC')


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi:
        try:
            cpuserial = "0000000000000000"
            f = open('/proc/cpuinfo', 'r')
            for line in f:
                if line[0:6] == 'Serial':
                    cpuserial = line[10:26].lstrip('0')
            f.close()
            return cpuserial
        except Exception as e:
            print(e)
    return '12345678'


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    cmd = "ps -aef | grep -i '%s' | grep -v 'grep' | awk '{ print $3 }'" % proc_name
    try:
        if len(os.popen(cmd).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print(f'Failed to get status of the process({proc_name}) - {e}')
    return is_running


def get_ip_address(ifname='wlan0'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if platform.system() == 'Windows':
        return '192.168.1.170'
    if not is_rpi:
        ifname = 'enp3s0'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        print(f'Failed to get IP address of {ifname}, reason: {e}')


def get_current_ap():
    """
    Get SSID of the current AP
    :return:
    """
    if is_rpi:
        pipe = os.popen('/sbin/iwgetid -r')
        ap = pipe.read().strip()
        pipe.close()
        return ap
    else:
        return "Testing AP"


def get_mac_address(interface='wlan0'):
    try:
        mac_str = open('/sys/class/net/' + interface + '/address').read()
    except Exception as e:
        logger.error(f'Failed to get MAC address of {interface}: {e}')
        mac_str = "00:00:00:00:00:00"
    return mac_str[0:17]


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi:
        try:
            pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
            data = pipe.read().strip()
            pipe.close()
            return data
        except OSError:
            logger.error('!!! Failed to get free GPU size! ')
            tot, used, free = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
            logger.error(f'Total: {tot}, Used: {used}, Free: {free}')
    return 0


def disable_screen_saver():
    if is_rpi:
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


def check_internet_connection():
    """
    Check internet connection
        It will be faster to just make a HEAD request so no HTML will be fetched.
        Also I am sure google would like it better this way :)
    """
    conn = http.client.HTTPConnection('www.google.com', timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except socket.error:
        conn.close()
        return False


def kill_process_by_name(proc_name, args="", use_sudo=False, sig=None):
    sig = signal.SIGKILL if sig is not None else signal.SIGKILL
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line and (not args or args in line):
            pid = int(line.split(None, 1)[0])
            logger.info('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            if use_sudo:
                os.system(f'sudo kill -9 {pid}')
            else:
                os.kill(pid, sig)
