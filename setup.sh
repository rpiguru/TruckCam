echo "===== Setting up TruckCam ====="
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt update
sudo apt install -y python3-dev python3-pip

sudo pip3 install -U setuptools pip
sudo pip3 install -r ${cur_dir}/requirements.txt

sudo apt install -y v4l-utils ffmpeg screen

echo "gpu_mem=256" | sudo tee -a /boot/config.txt

# Enable auto start
sudo sed -i -- "s/^exit 0/su pi -c \'screen -mS cam -d\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su pi -c \'screen -S cam -X stuff \"cd ${cur_dir////\\/}\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su pi -c \'screen -S cam -X stuff \"python3 main.py\\\\r\"\'\\nexit 0/g" /etc/rc.local
